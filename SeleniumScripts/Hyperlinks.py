from selenium import webdriver

driver = webdriver.Chrome(executable_path="D:\\Drivers\\chromedriver.exe")
driver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_links_w3schools")
driver.switch_to.frame("iframeResult")
driver.find_element_by_link_text("Visit W3Schools.com!").click()
driver.switch_to.default_content()
driver.find_element_by_css_selector("button[class='w3-button w3-bar-item w3-hover-white w3-hover-text-green']").click()
driver.switch_to.frame("iframeResult")
driver.find_element_by_partial_link_text("Visit").click()

