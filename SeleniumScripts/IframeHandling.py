from selenium import webdriver

driver = webdriver.Chrome(executable_path="D:\\Drivers\\chromedriver.exe")
driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_test")

driver.switch_to.frame("iframeResult")
FirstNameField=driver.find_element_by_id("fname")
FirstNameField.send_keys("Pranoday")

LastNameField=driver.find_element_by_id("lname")
LastNameField.send_keys("Dingare")

FirstName=FirstNameField.get_attribute("value")
LastName=LastNameField.get_attribute("value")

SubmitBtn=driver.find_element_by_css_selector("input[type='submit']")
SubmitBtn.click()

DivElement=driver.find_element_by_css_selector("div[class='w3-container w3-large w3-border']")
DivText=DivElement.text

if(FirstName in DivText and LastName in DivText):
    print("Data submitted successfully..PASSSED")
else:
    print("Data not submitted successfully..FAILED")
driver.switch_to.default_content()
#TAGNAME[ATTRIBUTENAME='ATTRIBUTEVALUE']
RunButton=driver.find_element_by_css_selector("button[class='w3-button w3-bar-item w3-hover-white w3-hover-text-green']")
RunButton.click()
