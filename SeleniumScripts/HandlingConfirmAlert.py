from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
NameField=driver.find_element_by_id("name")
NameField.send_keys("Pranoday")
EnteredName=NameField.get_attribute("value")
Al=driver.switch_to.alert
if(EnteredName in Al.text):
    print("Correct message is shown in confirmation..PASSED")
else:
    print("Correct message is NOT shown in confirmation..FAILED")
Al.dismiss()

