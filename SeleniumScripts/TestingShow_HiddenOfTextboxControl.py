import time

from selenium import webdriver

driver = webdriver.Chrome(executable_path="D:\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
HideButton=driver.find_element_by_id("hide-textbox")
HideButton.click()
time.sleep(5)
TxtBox1=driver.find_element_by_name("show-hide")
DisplayedState=TxtBox1.is_displayed()
if(DisplayedState==False):
    print("Hidden button worked fine")
else:
    print("Hidden button did not work fine")

ShowButton=driver.find_element_by_id("show-textbox")
ShowButton.click()
time.sleep(5)
DisplayedState=TxtBox1.is_displayed()
if(DisplayedState==True):
    print("Show button worked fine")
else:
    print("Show button did not work fine")
