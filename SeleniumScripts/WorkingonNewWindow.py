from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")

driver.find_element_by_id("openwindow").click()
AllHandles=driver.window_handles
for Handle in AllHandles:
    driver.switch_to.window(Handle)
    if("Home Page" in driver.title):
        break;
driver.find_element_by_link_text("Sign In").click()

