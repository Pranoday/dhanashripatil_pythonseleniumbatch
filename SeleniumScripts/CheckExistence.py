from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
Hidebutton=driver.find_element_by_id("hide-textbox")
Hidebutton.click()
IsElementExists=True
try:
    driver.find_element_by_id("show-textbox")
except NoSuchElementException:
    IsElementExists=False

if IsElementExists==False:
    print("Hide button worked fine")
else:
    print("Hide button did not work fine")


IsElementExists=True
ShowButton=driver.find_element_by_id("show-textbox")
ShowButton.click()

try:
    driver.find_element_by_id("show-textbox")
except NoSuchElementException:
    IsElementExists=False

if IsElementExists==True:
    print("Show button worked fine")
else:
    print("Show button did not work fine")