from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
AllCheckboxes=driver.find_elements_by_css_selector("input[type='checkbox']")
for checkbox in AllCheckboxes:
    checkbox.click()
    if(checkbox.is_selected()==True):
        print("Checkbox got selected after clicking..PASSED")
    else:
        print("Checkbox did not get selected after clicking..FAILED")
