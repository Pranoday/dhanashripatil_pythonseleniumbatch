from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://opensource-demo.orangehrmlive.com/")
driver.find_element_by_id("txtUsername").send_keys("admin123")
PasswordElement=driver.find_element_by_id("txtPassword")
Act=ActionChains(driver)
Act.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
Act.key_down(Keys.CONTROL).send_keys("c").key_up(Keys.CONTROL).perform()
Act.key_down(Keys.CONTROL,element=PasswordElement).send_keys("v").key_up(Keys.CONTROL).perform()
PastedText=PasswordElement.get_attribute("value")
if(PastedText==''):
    print("Text can not be pasted in Password field..PASSED")
else:
    print("Text can  be pasted in Password field..FAILED")