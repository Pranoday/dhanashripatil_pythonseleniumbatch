from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
DropdownElement=driver.find_element_by_id("carselect")
se=Select(DropdownElement)
DefaultSelectedOption=se.first_selected_option
if(DefaultSelectedOption.text=="BMW"):
    print("Correct default value is selected..PASSED")
else:
    print("Correct default value is NOT selected..PASSED")

se.select_by_value("benz")
time.sleep(3)
se.select_by_visible_text("Honda")
time.sleep(3)
se.select_by_index(2)
time.sleep(3)
SelectedOption=se.first_selected_option
if(SelectedOption.text=="Benz"):
    print("Correct option is selected..PASSED")
else:
    print("Correct option is NOT selected..FAILED")

ExpectedCarNames=["BMW","Benz","Honda"]
AllOptions=se.options
for Option in AllOptions:
    if Option.text in ExpectedCarNames:
        print("Corect option is present..PASSED")
    else:
        print("Corect option is NOT present..FAILED")