from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
PriceElement=driver.find_element_by_xpath("//table[@id='product']/descendant::td[text()='Python Programming Language ']/following-sibling::td")
print(PriceElement.text)