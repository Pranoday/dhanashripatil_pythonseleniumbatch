from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
BmwRadio=driver.find_element_by_id("bmwradio")
BmwRadio.click()
IsBMWChecked=BmwRadio.is_selected()
if(IsBMWChecked==True):
    print("Radio control got selected after clicking..PASSED")
else:
    print("Radio control did not get selected after clicking..FAILED")

BenzRadio=driver.find_element_by_id("benzradio")
BenzRadio.click()
IsBenzChecked=BenzRadio.is_selected()
IsBMWChecked=BmwRadio.is_selected()
if(IsBenzChecked==True and IsBMWChecked==False):
    print("Radio controls working fine..PASSED")
else:
    print("Radio controls not working fine..FAILED")

