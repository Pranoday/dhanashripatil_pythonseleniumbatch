import time

from selenium import webdriver

driver = webdriver.Chrome(executable_path="D:\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")

driver.find_element_by_id("hide-textbox").click()
ElementList=driver.find_elements_by_id("displayed-text")

if(len(ElementList)==0):
    print("Hide button worked fine")
else:
    print("Hide button did not work fine")

driver.find_element_by_id("show-textbox").click()
ElementList=driver.find_elements_by_id("displayed-text")
if(len(ElementList)==1):
    print("Show button worked fine")
else:
    print("Show button did not work fine")