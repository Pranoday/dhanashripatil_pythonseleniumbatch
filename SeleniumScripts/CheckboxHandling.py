from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
CheckboxElement=driver.find_element_by_id("bmwcheck")
CheckboxElement.click()
IsChecked=CheckboxElement.is_selected()
if(IsChecked==True):
    print("Checkbox got checked after clicking on it...PASSED")
else:
    print("Checkbox did not get checked after clicking on it...FAILED")

CheckboxElement.click()
IsChecked=CheckboxElement.is_selected()
if(IsChecked==False):
    print("Checked checkbox got Unchecked after clicking on it...PASSED")
else:
    print("Checked checkbox did not get Unchecked after clicking on it...FAILED")

