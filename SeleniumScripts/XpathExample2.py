from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch\\Drivers\\chromedriver.exe")
driver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_table_intro")
driver.switch_to.frame("iframeResult")
CompanyNameElement=driver.find_element_by_xpath("//table/descendant::td[text()='Canada']/preceding-sibling::td[2]")
print(CompanyNameElement.text)