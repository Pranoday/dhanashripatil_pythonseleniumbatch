import csv

from selenium.webdriver.common.by import By

from AutomationFramework.Switcher import BrowserDriverSwitcher
from AutomationFramework.Utilities.Utils import Utils


class WebTest:
    Driver=None
    def __init__(self):
        #ObjectRepo is a dictionary saving WebElements against the Keys
        self.ObjectRepo={}


    def StartTest(self,BrowserName):
        self.OpenBrowser(BrowserName)
        self.OpenURL()

    def OpenBrowser(self,BrowserName):
        Func=BrowserDriverSwitcher.get(BrowserName)
        Func()
    def OpenURL(self):
        WebTest.Driver.get(Utils.EnvVars["ApplicationURL"])


    def EnterText(self,Element,TextToEnter):
        self.ClearText(Element)
        Element.send_keys(TextToEnter)

    def ClickElement(self,Element):
        Element.click()

    def AppendText(self,Element,TextToAppend):
        Element.send_keys(TextToAppend)

    def ClearText(self,Element):
        Element.clear()

    def SelectValueFromDropdown(self,Element,ValueSelectionStrategy,Value):
        pass

    def GetPageTitle(self):
        return WebTest.Driver.getTitle()

    def GetElementText(self,Element):
        return Element.text
    def GetURL(self):
        return WebTest.Driver.current_url

    def CreateObjectRepository(self,FileName):
        FilePath=Utils.EnvVars["ObjectRepositoriesLocation"]+"\\"+FileName+".csv"
        with open(FilePath) as ObjectRepo:
            csv_reader = csv.reader(ObjectRepo, delimiter=',')
            for row in csv_reader:
                El = self.FindAndReturnElement(row[1], row[2])
                self.ObjectRepo[row[0]]=El

    def FindAndReturnElement(self,StrategyToken,Locator):
        IdentificationData = self.CreateStratergyLocatorTuple(StrategyToken, Locator)
        RequiredElement=WebTest.Driver.find_element(*IdentificationData)
        return RequiredElement

    def CreateStratergyLocatorTuple(self,StratergyToken,Locator):
        StratergyLocatorTuple={
            "BY_ID":(By.ID,Locator),
            "BY_CLASS":(By.CLASS_NAME,Locator),
            "BY_NAME":(By.NAME,Locator),
            "BY_CSS":(By.CSS_SELECTOR,Locator),
            "BY_XPATH":(By.XPATH,Locator),
            "BY_LINKTEXT":(By.LINK_TEXT,Locator)


        }
        return StratergyLocatorTuple.get(StratergyToken)
