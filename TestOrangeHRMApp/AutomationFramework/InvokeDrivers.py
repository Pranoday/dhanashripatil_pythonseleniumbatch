from selenium import webdriver

from AutomationFramework.Utilities.Utils import Utils



def StartChromeDriver():

    from AutomationFramework.WebTest import WebTest
    WebTest.Driver = webdriver.Chrome(
        executable_path=Utils.EnvVars.get("ChromeDriverPath"))

def StartFirefoxDriver():
    from AutomationFramework.WebTest import WebTest
    WebTest.Driver = webdriver.Firefox(
        executable_path=Utils.EnvVars.get("FirefoxDriverPath"))