from AutomationFramework.WebTest import WebTest


class BasePage():
    def __init__(self):
        self.T=WebTest()

    def GetPageTitle(self):
        return self.T.GetPageTitle()

    def GetPageURL(self):
        return self.T.GetURL()