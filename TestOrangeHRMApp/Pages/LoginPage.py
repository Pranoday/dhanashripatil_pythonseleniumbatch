from AutomationFramework.Utilities.Utils import Utils
from Pages.BasePage import BasePage


class LoginPage(BasePage):

    def __init__(self):
        super().__init__()
        Utils.InitialiseEnvVars()
        self.T.StartTest("Chrome")
        self.T.CreateObjectRepository("LoginPage")
        #self.UserNameField=driver.findelementbyid("txtUsername")
        #self.PasswordField=driver.findelementbyid("txtPassword")
        #self.LoginButton=driver.findElementById("SignIn")

        #Element finding will be done here

    def DoLogin(self,UserName,Password):

        self.EnterUserName(UserName)
        self.EnterUserPassword(Password)
        self.ClickLoginButton()
        #Login process will be implemented here

    def EnterUserName(self,UserName):
        #self.UserNameField.send_keys(UserName)
        self.T.EnterText(self.T.ObjectRepo["UserNameField"],UserName)
        #Enter Username in UserName field

    def EnterUserPassword(self,Password):
        #self.PasswordField.send_keys(Password)
        #Enter Password in Password field
        self.T.EnterText(self.T.ObjectRepo["PasswordField"],Password)

    def ClickLoginButton(self):
        #self.LoginButton.click()
        #Click on Login Button
        self.T.ClickElement(self.T.ObjectRepo["LoginButton"])

    def GetLoginErrorMessage(self):
        ErrorMessageElement=self.T.FindAndReturnElement("BY_ID","spanMessage")
        ErrorMessage=self.T.GetElementText(ErrorMessageElement)
        return ErrorMessage