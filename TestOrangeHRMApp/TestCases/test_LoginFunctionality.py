'''
    1.Open Browser
    2.Open URL:https://opensource-demo.orangehrmlive.com/
    3.Enter "Admin" in Username field
    4.Enter "admin123" in Password field
    5.Click on LOGIN button
    6.Check if Welcome screen opens or not
'''
from Pages.LoginPage import LoginPage


class TestLoginfunctionality:
    def testLoginFunctionalityWithValidCredentials(self):
        loginpage=LoginPage()
        loginpage.DoLogin("Admin","admin123")
        PageURL=loginpage.GetPageURL()
        assert PageURL=="","Dashboard page not opened after entering valid credentials"
    def testLoginFunctionalityWithInValidCredentials(self):
        loginpage = LoginPage()
        loginpage.DoLogin("", "")
        ErrorMessage=loginpage.GetLoginErrorMessage()
        assert ErrorMessage=="Username cannot be empty","Wrong error message is shown"